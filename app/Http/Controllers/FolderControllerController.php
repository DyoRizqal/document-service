<?php

namespace App\Http\Controllers;

use App\Models\FolderController;
use App\Models\DocumentController;
use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class FolderControllerController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $folders = FolderController::all();
        $documents = DocumentController::all();
        foreach($folders as $folder){
            if($folder->is_public=='true'){
                $folderrr = FolderController::where('is_public','true')
                ->select('id','name','type','is_public','owner_id',
                        'share','timestamp','company_id')->get();
                $documenttt = DocumentController::where('is_public','true')
                ->select('id','name','type','is_public','owner_id',
                'share','timestamp','company_id')->get();
            }
            $share = json_decode($folder->share);
            foreach((array)$share as $share_id){
                if($folder->is_public=='false' || $folder->owner_id == Auth::user()->id || $share_id == Auth::user()->id){
                   $folderr = FolderController::where('is_public','false')
                   ->where('share', Auth::user()->id)->orWhere('owner_id',Auth::user()->id)
                   ->select('id','name','type','is_public','owner_id',
                           'share','timestamp','company_id')->get();
                   $documentt = DocumentController::where('is_public','false')->orWhere('owner_id',Auth::user()->id)
                   ->where('share', Auth::user()->id)
                   ->select('id','name','type','is_public','owner_id',
                   'share','timestamp','company_id')->get();
                }
            }
        }
        if($folder->owner_id == Auth::user()->id){
            return response()->json([$folderr,$documentt]);
        }
        else{
            return response()->json([$folderrr,$folderr,$documenttt,$documentt]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 'uniqid'->str::random(9).'-'str::random
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = FolderController::find($request->input('id'));
        if($check == null){
            // DB::enableQueryLog();
            $folder = new FolderController();
                $data = [
                    'id' => $request->input('id'),
                    'name' => $request->input('name'),
                    'timestamps' => $request->input('timestamp'),
                    'type' => 'folder',
                    'content' => '{}',
                    'owner_id' => Auth::user()->id,
                    'company_id' => Auth::user()->id
                ];
                $save = $folder->create($data);
                $message = "created";
            // dd(DB::getQueryLog($check));
        }
        else{
            $data = [
                'name' => $request->input('name'),
                'timestamps' => $request->input('timestamp'),
                'type' => 'folder',
                'content' => '{}',
                'owner_id' => Auth::user()->id,
                'company_id' => Auth::user()->id
            ];
            $save = $check->update($data);
            $message = "updated";
        }
        if($save){
            return response()->json(['error'=>'false', 'message' => 'Folder '.$message, 'data' => $data]);
        }else{
            return response()->json(['error'=>'true', 'message' => 'Failed']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FolderController  $folderController
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // DB::enableQueryLog();
        $id = $request->input('id');
        $folders = DocumentController::where('folder_id',$id)->get();
        // dd(DB::getQueryLog());
        // dd(count($folders));
    
        if (!$folders) {
            return response()->json(['success' => false,'message' => 'Sorry, Folders not found.']);
        }
      // foreach($folders as $folder){
        $count = count($folders);
        for($x = 0; $x < $count; $x++){
            $data1 = [
                'id' => $folders[$x]->id,
                'name' => $folders[$x]->name,
                'type' => $folders[$x]->type,
                'folder_id' => $folders[$x]->folder_id,
                'content' => [
                    'blocks' => [array(
                        'type' => $folders[$x]->type_text,
                        'text' => $folders[$x]->text,
                    )]
            ],
                'timestamp' => $folders[$x]->timestamp,
                'owner_id' => $folders[$x]->owner_id,
                'share' => json_encode($folders[$x]->share)
        ];
            $data2 = [
                'id' => $folders[$x]->id,
                'name' => $folders[$x]->name,
                'type' => $folders[$x]->type,
            ];
            echo $folders[$x];
            // return response()->json([$data1, $data2]); die();
        }
        
        // return response()->json([
        //     'error' => false,
        //     'data' => [
        //         'id' => $folders[0]->id,
        //         'name' => $folders[0]->name,
        //         'type' => $folders[0]->type,
        //         'folder_id' => $folders[0]->folder_id,
        //         'content' => [
        //             'blocks' => [array(
        //                 'type' => $folders[0]->type_text,
        //                 'text' => $folders[0]->text,
        //             )]
        //         ],
        //         'timestamp' => $folders[0]->timestamp,
        //         'owner_id' => $folders[0]->owner_id,
        //         'share' => json_encode($folders[0]->share)
        //     ],
        //     [
        //         'id' => $folders[0]->id,
        //         'name' => $folders[0]->name,
        //         'type' => $folders[0]->type,
        //     ]
        // ]);
        
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FolderController  $folderController
     * @return \Illuminate\Http\Response
     */
    public function edit(FolderController $folderController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FolderController  $folderController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FolderController $folderController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FolderController  $folderController
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $hapus =  DB::table('folder_controllers')->where('id',$id)->delete();  
        return response()->json(['error' => false,'message' => 'Success delete folder']);
    }
}
