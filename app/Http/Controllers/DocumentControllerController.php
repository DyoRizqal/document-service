<?php

namespace App\Http\Controllers;

use App\Models\DocumentController;
use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DocumentControllerController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $document = new DocumentController();
                $data = [
                    'id' => $request->input('id'),
                    'name' => $request->input('name'),
                    'type' => 'document',
                    'folder_id' => $request->input('folder_id'),
                    'timestamps' => $request->input('timestamp'),
                    'owner_id' => Auth::user()->id,
                    'share' => $request->input('share'),
                    'company_id' => Auth::user()->id
                ];
                $save = $document->create($data);
                $message = "set";
        if($save){
            return response()->json(['error'=>'false', 'message' => 'Succes '.$message, 'data' => $data]);
        }else{
            return response()->json(['error'=>'true', 'message' => 'Failed']);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentController  $documentController
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->input('id');
        $documents = DocumentController::where('id',$id)->get();  
        if (!$documents) {
            return response()->json(['success' => false,'message' => 'Sorry, Document not found.']);
        }
         return response()->json([
            'error' => false,
            'message' => 'Succes get document',
            'data' => [
                'id' => $documents[0]->id,
                'name' => $documents[0]->name,
                'type' => $documents[0]->type,
                'folder_id' => $documents[0]->folder_id,
                'content' => [
                    'blocks' => [array(
                        'type' => $documents[0]->type_text,
                        'text' => $documents[0]->text,
                    )]
                ],
                'timestamp' => $documents[0]->timestamp,
                'owner_id' => $documents[0]->owner_id,
                'share' => explode(',',$documents[0]->share)
            ],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocumentController  $documentController
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentController $documentController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentController  $documentController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentController $documentController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentController  $documentController
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $hapus =  DB::table('document_controllers')->where('id',$id)->delete();  
        return response()->json(['error' => false,'message' => 'Success delete document']);
    }
}
