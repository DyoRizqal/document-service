<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FolderController extends Model
{
    use HasFactory;
    protected $table = 'folder_controllers';
    protected $fillable = [
        'id', 'name', 'type', 'is_public','owner_id', 'share', 'timestamp','company_id', 'content'
    ];
}
