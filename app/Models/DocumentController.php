<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentController extends Model
{
    use HasFactory;
    protected $table = 'document_controllers';
    protected $fillable = [
        'id', 'name', 'type', 'folder_id','type_text', 'text', 'is_public', 'timestamp','owner_id', 'share', 'company_id'
    ];
}
