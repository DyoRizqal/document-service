<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentControllersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_controllers', function (Blueprint $table) {
            $table->increments('uniqid');
            $table->string('id');
            $table->string('name');
            $table->string('type');
            $table->string('folder_id');
            $table->string('type_text');
            $table->string('text');
            $table->string('is_public');
            $table->string('timestamp');
            $table->string('owner_id');
            $table->string('share');
            $table->string('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_controllers');
    }
}
