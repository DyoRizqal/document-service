<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFolderControllersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folder_controllers', function (Blueprint $table) {
            $table->increments('uniqid');
            $table->string('id');
            $table->string('name');
            $table->string('type');
            $table->string('is_public');
            $table->string('owner_id');
            $table->string('share');
            $table->string('timestamp');
            $table->string('company_id');
            $table->string('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folder_controllers');
    }
}
